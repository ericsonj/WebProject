<%-- 
    Document   : index
    Created on : Nov 4, 2017, 7:02:55 PM
    Author     : ejoseph
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="WEB-INF/tlds/ericsonjtags.tld" prefix="ericsonj"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <ericsonj:css></ericsonj:css>
        <ericsonj:script></ericsonj:script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
            <title>Web Project</title>
        </head>
        <body>
        <ericsonj:ifsessionok>
            <ericsonj:navbar></ericsonj:navbar>
        </ericsonj:ifsessionok>   
        <ericsonj:content></ericsonj:content>
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <p>Ericsonj Dev © 2017<p>
                        
                        <a href="mailto:ericsonjoseph@gmail.com"><span class="glyphicon glyphicon-envelope"></span> ericsonjoseph@gmail.com</a>
                </div>
            </div>
        </footer>
    </body>
</html>
