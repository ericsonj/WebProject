package net.ericsonj.web.controller;

import java.io.IOException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.ModelAndView;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.framework.web.mvc.ViewController;
import net.ericsonj.framework.web.mvc.ViewSuccess;
import net.ericsonj.save.elements.Localization;
import net.ericsonj.save.facade.LocalizationDaoFacade;
import net.ericsonj.web.view.LocalizationEditAddView;
import net.ericsonj.web.view.LocalizationCRUDView;

/**
 *
 * @author ejoseph
 */
public class LocalizationController extends ViewController {

    @Override
    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) {

        String opt = request.getParameter("opt");
        
        if (request.getMethod().equalsIgnoreCase("GET")) {
            String edit = request.getParameter("edit");
            String sid = request.getParameter("id");

            if (edit != null && sid != null) {
                long id = Long.parseLong(sid);
                ModelAndView mEdit = new ModelAndView(new LocalizationEditAddView(opt));
                mEdit.addObject("id", id);
                return mEdit;
            }
            String add = request.getParameter("add");
            if (add != null) {
                ModelAndView addView = new ModelAndView(new LocalizationEditAddView(opt));
                addView.addObject("add", "add");
                return addView;
            }

        }

        if (request.getMethod().equalsIgnoreCase("POST")) {
            String edit = request.getParameter("edit");
            String add = request.getParameter("add");
            if (edit != null || add != null) {
                LinkedList<ValidateException> exceptions = validRequestForm(request);
                if (!exceptions.isEmpty()) {
                    ModelAndView mEdit = new ModelAndView(new LocalizationEditAddView(opt));
                    mEdit.addObject("request", request);
                    mEdit.addObject("formError", "error");
                    for (ValidateException exception : exceptions) {
                        mEdit.addObject(exception.getId(), exception.getDesc());
                    }
                    return mEdit;
                }
            }
            if (edit != null) {
                AbstractView view = ProcessEditForm(request);
                return new ModelAndView(view);
            }

            if (add != null) {
                AbstractView view = ProcessAddForm(request);
                return new ModelAndView(view);
            }

        }

        ModelAndView mav = new ModelAndView(new LocalizationCRUDView(opt));
        return mav;
    }

    @Override
    public String handleAjaxPOSTRequest(HttpServletRequest request, HttpServletResponse response) {
        String sessionUser = (String) request.getSession().getAttribute("session-user");
        if (sessionUser == null) {
            try {
                response.sendError(401, "Unauthorized");
            } catch (IOException ex) {
                Logger.getLogger(LocalizationController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }
//        LocalizationDTAjax ldta = new LocalizationDTAjax(request, response);
//        ldta.run();
        return "";
    }

    protected LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {

        LinkedList<ValidateException> exceptions = new LinkedList<>();
        String townCode = request.getParameter("town_code");
        System.out.println("townCode : " + townCode);
        if (!fieldValidByRegex("^[0-9]+$", townCode)) {
            exceptions.add(new ValidateException("town_code", "Ingrese solo numeros"));
        }

        String depCode = request.getParameter("department_code");
        System.out.println("department_code : " + depCode);
        if (!fieldValidByRegex("^[0-9]+$", depCode)) {
            exceptions.add(new ValidateException("department_code", "Ingrese solo numeros"));
        }

        return exceptions;
    }

    private boolean fieldValidByRegex(String regex, String field) {
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(field);
        return m.find();
    }
    
    protected AbstractView ProcessEditForm(HttpServletRequest request) {
        String town_code = request.getParameter("town_code");
        String department_code = request.getParameter("department_code");
        String department_name = request.getParameter("department_name");
        String town_name = request.getParameter("town_name");

        Localization loc = new Localization();
        loc.setTown_code(Long.parseLong(town_code));
        loc.setDepartment_code(Long.parseLong(department_code));
        loc.setDepartment_name(department_name);
        loc.setTown_name(town_name);

        LocalizationDaoFacade facade = new LocalizationDaoFacade();
        facade.edit(loc);

        return new ViewSuccess("Localizations", "La localizacion fue editada exitosamente.", "index.jsp?opt=net.ericsonj.web.controller.LocalizationController");
    }

    private AbstractView ProcessAddForm(HttpServletRequest request) {
        String town_code = request.getParameter("town_code");
        String department_code = request.getParameter("department_code");
        String department_name = request.getParameter("department_name");
        String town_name = request.getParameter("town_name");

        Localization loc = new Localization();
        loc.setTown_code(Long.parseLong(town_code));
        loc.setDepartment_code(Long.parseLong(department_code));
        loc.setDepartment_name(department_name);
        loc.setTown_name(town_name);

        LocalizationDaoFacade facade = new LocalizationDaoFacade();
        facade.create(loc);

        return new ViewSuccess("Localizations", "La localizacion fue creada exitosamente.", "index.jsp?opt=net.ericsonj.web.controller.LocalizationController");
    }

}
