package net.ericsonj.web.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.ericsonj.framework.web.mvc.AbstractController;
import net.ericsonj.web.view.IndexView;

/**
 *
 * @author ejoseph
 */
public class IndexController extends AbstractController {

    @Override
    public String handleRequestPOST(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String handleRequestGET(HttpServletRequest request, HttpServletResponse response) {

        String logout = request.getParameter("logout");
        if (logout != null) {
            request.getSession().invalidate();
            try {
                response.sendRedirect("index.jsp");
            } catch (IOException ex) {
                Logger.getLogger(IndexController.class.getName()).log(Level.SEVERE, null, ex);
            }
            return "";
        }

        IndexView iv = new IndexView();
        iv.addObject("username", request.getSession().getAttribute("session-user"));
        return iv.getHtml();
    }

    @Override
    public String handleRequestMETHOD(String method, HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
