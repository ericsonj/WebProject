package net.ericsonj.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.ericsonj.framework.web.mvc.AbstractController;
import net.ericsonj.web.view.UserView;

/**
 *
 * @author ejoseph
 */
public class UserController extends AbstractController {

    @Override
    public String handleRequestPOST(HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String handleRequestGET(HttpServletRequest request, HttpServletResponse response) {
        UserView uv = new UserView();
        return uv.getHtml();
    }

    @Override
    public String handleRequestMETHOD(String method, HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
