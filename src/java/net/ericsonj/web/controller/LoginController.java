package net.ericsonj.web.controller;

import net.ericsonj.web.view.LoginView;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.ericsonj.aaa.EricsonjAAA;
import net.ericsonj.framework.web.mvc.AbstractController;
import org.hibernate.HibernateException;

/**
 *
 * @author ejoseph
 */
public class LoginController extends AbstractController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    @Override
    public String handleRequestPOST(HttpServletRequest request, HttpServletResponse response) {
        System.out.println("---------------------------------");
        System.out.println("URL: " + request.getRequestURI());
        Map<String, String[]> paramMap = request.getParameterMap();
        for (String key : paramMap.keySet()) {
            System.out.println("KEY: " + key + " VALUE: " + Arrays.toString(paramMap.get(key)) + " NUM_VALUES: " + paramMap.get(key).length);
        }

        HttpSession session = request.getSession();
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (username != null && password != null) {

//            UserDaoFacade uFacade = new UserDaoFacade();
            EricsonjAAA aaa = new EricsonjAAA(username, password);
            boolean isValidate = false;
            try {
                isValidate = aaa.isAuthorised();
            } catch (HibernateException ex) {
                LoginView indexView = new LoginView();
                indexView.addObject("error", ex.toString());
                return indexView.getHtml();
            }

            if (isValidate) {
                session.setAttribute("session-user", username);
                session.setAttribute("auser", aaa.getUserObject());
                
                String requestParam = (String) session.getAttribute("request-param");
                if (requestParam == null) {
                    requestParam = "";
                }
                try {
                    response.sendRedirect(request.getRequestURI() + requestParam);
                    return "";
                } catch (IOException ex) {
                    LOG.log(Level.SEVERE, null, ex);
                }
            } else {
                LoginView indexView = new LoginView();
                indexView.addObject("error", " The username or password you entered are not correct. Please try again.");
                return indexView.getHtml();
            }
        }
        return "";
    }

    @Override
    public String handleRequestGET(HttpServletRequest request, HttpServletResponse response) {

        System.out.println("URL: " + request.getRequestURI());
        Map<String, String[]> paramMap = request.getParameterMap();
        StringBuilder sb = new StringBuilder();
        sb.append("?");
        for (String key : paramMap.keySet()) {
            String[] ss = paramMap.get(key);
            for (String s : ss) {
                sb.append(key);
                sb.append("=");
                sb.append(s);
                sb.append("&");
            }
        }
        sb.deleteCharAt(sb.length() - 1);

        String requestParam = sb.toString();
        System.out.println("PARAM:|" + requestParam + "|");

        HttpSession session = request.getSession();
        if (!requestParam.isEmpty()) {
            session.setAttribute("request-param", requestParam);
        }
        String sessionUser = (String) session.getAttribute("auser");
        if (sessionUser == null) {
            LoginView indexView = new LoginView();
            return indexView.getHtml();
        }
        return "";
    }

    @Override
    public String handleRequestMETHOD(String method, HttpServletRequest request, HttpServletResponse response) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
