package net.ericsonj.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.ericsonj.framework.web.mvc.ModelAndView;
import net.ericsonj.framework.web.mvc.ViewController;

/**
 *
 * @author ejoseph
 */
public class MyJspController extends ViewController {

    @Override
    public ModelAndView handleRequestInternal(HttpServletRequest request, HttpServletResponse response) {
        String method = request.getMethod();
        System.out.println("METHOD: " + method);
        ModelAndView model = new ModelAndView("view-test.jsp");
        model.addObject("name", "Ericson");
        model.addObject("lastName", "Estupinan");
        model.addObject("email  ", "ericsonjoseph@gmail.com");
        return model;
    }

    @Override
    public boolean requireSession() {
        return false;
    }
    
}
