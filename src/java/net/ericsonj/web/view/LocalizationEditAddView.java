package net.ericsonj.web.view;

import net.ericsonj.framework.web.mvc.EditAddView;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.bootstrap.TextFormField;
import net.ericsonj.html.HtmlElement;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Localization;
import net.ericsonj.save.facade.LocalizationDaoFacade;

/**
 *
 * @author ejoseph
 */
public class LocalizationEditAddView extends EditAddView<Localization, Long> {

    public LocalizationEditAddView(String opt) {
        super(opt);
    }

    @Override
    public void initForm(AdministrableForm form, Localization loc) {

        TextFormField field1 = new TextFormField("town_code", "Codigo de municipio:");
        field1.setHelpMessage("Ingrese codigo dane de minicipio");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-2 col-md-2 col-sm-3");
        if (loc != null) {
            field1.setValue("" + loc.getTown_code());
            field1.getInput().setReadonly();
        }
        field1.getInput().addAttibute("maxlength", "5");

        form.addFormField("town_code", field1);

        field1 = new TextFormField("department_code", "Codigo Departamento:");
        field1.setHelpMessage("Ingrese codigo del departamento");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-2 col-md-2 col-sm-3");
        field1.getInput().addAttibute("maxlength", "5");
        if (loc != null) {
            field1.setValue("" + loc.getDepartment_code());
        }

        form.addFormField("department_code", field1);

        field1 = new TextFormField("department_name", "Departamento:");
        field1.setHelpMessage("Ingrese el nombre del departamento");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        field1.getInput().addAttibute("maxlength", "30");
        if (loc != null) {
            field1.setValue("" + loc.getDepartment_name());
        }

        form.addFormField("department_name", field1);

        field1 = new TextFormField("town_name", "Municipio:");
        field1.setHelpMessage("Ingrese el nombre del municipio.");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        field1.getInput().addAttibute("maxlength", "30");
        if (loc != null) {
            field1.setValue("" + loc.getTown_name());
        }

        form.addFormField("town_name", field1);

    }

    @Override
    public String getTitle() {
        return "Localization";
    }

    @Override
    public ElementLogicFacade getFacade() {
        return new LocalizationDaoFacade();
    }

    @Override
    public Class<Localization> getObjectType() {
        return Localization.class;
    }

    @Override
    public HtmlElement getIconForm() {
        return null;
    }

}
