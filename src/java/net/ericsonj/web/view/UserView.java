package net.ericsonj.web.view;

import java.util.List;
import net.ericsonj.framework.bootstrap.Container;
import net.ericsonj.framework.bootstrap.Modal;
import net.ericsonj.framework.bootstrap.PanelPrimary;
import net.ericsonj.html.A;
import net.ericsonj.html.Button;
import net.ericsonj.html.Div;
import net.ericsonj.html.H3;
import net.ericsonj.html.HtmlContainer;
import net.ericsonj.html.Script;
import net.ericsonj.html.Span;
import net.ericsonj.html.Table;
import net.ericsonj.html.Tbody;
import net.ericsonj.html.Text;
import net.ericsonj.html.Th;
import net.ericsonj.html.Thead;
import net.ericsonj.html.Tr;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.save.elements.User;
import net.ericsonj.save.facade.UserDaoFacade;

/**
 *
 * @author ejoseph
 */
public class UserView extends AbstractView {

    @Override
    public String getHtml() {

        Container cont = new Container();

        Div head = new Div("panel-heading");
        head.addElement(new H3("Users"));
        Div body = new Div("panel-body");

        Table table = new Table();
        table.setClass("table table-striped table-bordered");
        table.setId("localization");

        HtmlContainer thead = new Thead().addElement(new Tr()
                .addElement(new Th().addElement(new Text("Login")))
                .addElement(new Th().addElement(new Text("Name")))
                .addElement(new Th().addElement(new Text("Email")))
                .addElement(new Th().addElement(new Text("deleted")))
                .addElement(new Th().addElement(new Span("glyphicon glyphicon-cog"))));

        table.addElement(thead);

        Tbody tbody = new Tbody();

        UserDaoFacade lFacade = new UserDaoFacade();
        List<User> users = lFacade.findAll();

        for (User user : users) {
            Tr tr = new Tr();
            Th th = (Th) new Th().addElement(new Text(user.getLogin()));
            tr.addElement(th);
            th = (Th) new Th().addElement(new Text(user.getName()));
            tr.addElement(th);
            th = (Th) new Th().addElement(new Text(user.getEmail()));
            tr.addElement(th);
            th = (Th) new Th().addElement(new Text(Boolean.toString(user.isDeleted())));
            tr.addElement(th);

            A remove = (A) new A().addElement(new Span("glyphicon glyphicon-trash")).setClass("btn btn-danger btn-xs");
            A edit = (A) new A().addElement(new Span("glyphicon glyphicon-pencil")).setClass("btn btn-default btn-xs");

            Div cong = new Div();
            cong.addElement(edit);
            cong.addElement(new Text("  "));
            cong.addElement(remove);

            th = new Th();
            th.addElement(cong);

            tr.addElement(th);
            tbody.addElement(tr);
        }

        table.addElement(tbody);

        Script script = new Script();
        script.setType("text/javascript");
        script.setScript("$(document).ready(function() {\n"
                + "    $('#localization').DataTable();\n"
                + "} );");

        table.addElement(script);

//        A addButton = new A("Add User");
//        addButton.setClass("btn btn-success btn-crud-add");
//        addButton.setHref("?opt=net.ericsonj.web.co   ntroller.UserController&amp;action=add");
        Button addButton = new Button("Open Modal");
        addButton.setClass("btn btn-success btn-crud-add");
        addButton.setDataCustom("toggle", "modal");
        addButton.setDataCustom("target", "#myModal");
        body.addElement(addButton);

        Modal moda = new Modal("myModal", "dialog", "", true, true);

        body.addElement(moda.getElement());
        body.addElement(table);

        PanelPrimary panelPrimary = new PanelPrimary(head, body);

        cont.addElement(panelPrimary);
//        cont.addElement(script);

        return cont.getHtml();

    }

}
