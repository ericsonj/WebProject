package net.ericsonj.web.view;

import net.ericsonj.html.Canvas;
import net.ericsonj.html.Div;
import net.ericsonj.html.H1;
import net.ericsonj.html.Script;
import net.ericsonj.framework.web.mvc.AbstractView;

/**
 *
 * @author ejoseph
 */
public class IndexView extends AbstractView {

    @Override
    public String getHtml() {
        Div content = new Div("container");
        Div pageHeader = new Div("page-header");
        String username = (String) attMap.get("username");
        pageHeader.addElement(new H1("Welcome " + (username == null ? "" : username)));
        content.addElement(pageHeader);

        Div row = new Div("row");
//        Card card = new Card(
//                "User",
//                "Some quick example text to build on the card title and make up the bulk of the card's content.",
//                "img/card.png",
//                "card",
//                "Go",
//                "index.jsp?opt=net.ericsonj.web.controller.UserController");
//
//        Div cardDeck = new Div("card-deck");
//        cardDeck.addElement(card.getElement());
//        cardDeck.addElement(card.getElement());
//        cardDeck.addElement(card.getElement());
//
//        row.addElement(cardDeck);

        Div col = new Div("col-sm-4");

        Canvas canvas = new Canvas();
        canvas.setId("myChart");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        Script script = new Script();
        script.setType("application/javascript");
        script.setScript("var ctx = document.getElementById(\"myChart\");\n"
                + "var myChart = new Chart(ctx, {\n"
                + "    type: 'bar',\n"
                + "    data: {\n"
                + "        labels: [\"Red\", \"Blue\", \"Yellow\", \"Green\", \"Purple\", \"Orange\"],\n"
                + "        datasets: [{\n"
                + "            label: '# of Votes',\n"
                + "            data: [12, 19, 3, 5, 2, 3],\n"
                + "            backgroundColor: [\n"
                + "                'rgba(255, 99, 132, 0.2)',\n"
                + "                'rgba(54, 162, 235, 0.2)',\n"
                + "                'rgba(255, 206, 86, 0.2)',\n"
                + "                'rgba(75, 192, 192, 0.2)',\n"
                + "                'rgba(153, 102, 255, 0.2)',\n"
                + "                'rgba(255, 159, 64, 0.2)'\n"
                + "            ],\n"
                + "            borderColor: [\n"
                + "                'rgba(255,99,132,1)',\n"
                + "                'rgba(54, 162, 235, 1)',\n"
                + "                'rgba(255, 206, 86, 1)',\n"
                + "                'rgba(75, 192, 192, 1)',\n"
                + "                'rgba(153, 102, 255, 1)',\n"
                + "                'rgba(255, 159, 64, 1)'\n"
                + "            ],\n"
                + "            borderWidth: 1\n"
                + "        }]\n"
                + "    },\n"
                + "    options: {\n"
                + "        scales: {\n"
                + "            yAxes: [{\n"
                + "                ticks: {\n"
                + "                    beginAtZero:true\n"
                + "                }\n"
                + "            }]\n"
                + "        }\n"
                + "    }\n"
                + "});");

        col.addElement(canvas);
        col.addElement(script);

        canvas = new Canvas();
        canvas.setId("myChart2");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        script = new Script();
        script.setType("application/javascript");
        script.setScript("var ctx = document.getElementById('myChart2').getContext('2d');\n"
                + "var chart = new Chart(ctx, {\n"
                + "    // The type of chart we want to create\n"
                + "    type: 'line',\n"
                + "\n"
                + "    // The data for our dataset\n"
                + "    data: {\n"
                + "        labels: [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\"],\n"
                + "        datasets: [{\n"
                + "            label: \"My First dataset\",\n"
                + "            backgroundColor: 'rgba(255, 99, 132, 0.4)',\n"
                + "            borderColor: 'rgb(255, 99, 132)',\n"
                + "            data: [0, 10, 5, 2, 20, 30, 45],\n"
                + "        }]\n"
                + "    },\n"
                + "\n"
                + "    // Configuration options go here\n"
                + "    options: {}\n"
                + "});");

        col.addElement(canvas);
        col.addElement(script);

        row.addElement(col);

        col = new Div("col-sm-4");

        canvas = new Canvas();
        canvas.setId("chart-area");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        script = new Script();
        script.setType("application/javascript");
        script.setScript(""
                + "    var randomScalingFactor = function() {\n"
                + "        return Math.round(Math.random() * 100);\n"
                + "    };\n"
                + "\n"
                + "    var config = {\n"
                + "        type: 'pie',\n"
                + "        data: {\n"
                + "            datasets: [{\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                ],\n"
                + "                backgroundColor: ["
                + "                    'rgba(255, 99, 132, 0.5)',\n"
                + "                    'rgba(54, 162, 235, 0.5)',\n"
                + "                    'rgba(255, 206, 86, 0.5)',\n"
                + "                    'rgba(75, 192, 192, 0.5)',\n"
                + "                    'rgba(153, 102, 255, 0.5)'"
                + "                ],\n"
                + "                label: 'Dataset 1'\n"
                + "            }],\n"
                + "            labels: [\n"
                + "                \"Red\",\n"
                + "                \"Orange\",\n"
                + "                \"Yellow\",\n"
                + "                \"Green\",\n"
                + "                \"Blue\"\n"
                + "            ]\n"
                + "        },\n"
                + "        options: {\n"
                + "            responsive: true\n"
                + "        }\n"
                + "    };\n"
                + "\n"
                + " \n"
                + "        var ctx = document.getElementById(\"chart-area\").getContext(\"2d\");\n"
                + "        window.myPie = new Chart(ctx, config);\n"
                + "\n");

        col.addElement(canvas);
        col.addElement(script);

        canvas = new Canvas();
        canvas.setId("stacked-group");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        script = new Script();
        script.setType("application/javascript");
        script.setScript("var barChartData = {\n"
                + "            labels: [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\"],\n"
                + "            datasets: [{\n"
                + "                label: 'Dataset 1',\n"
                + "                backgroundColor: 'rgba(255, 99, 132, 0.8)',\n"
                + "                stack: 'Stack 0',\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor()\n"
                + "                ]\n"
                + "            }, {\n"
                + "                label: 'Dataset 2',\n"
                + "                backgroundColor: 'rgba(54, 162, 235, 0.8)',\n"
                + "                stack: 'Stack 0',\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor()\n"
                + "                ]\n"
                + "            }, {\n"
                + "                label: 'Dataset 3',\n"
                + "                backgroundColor: 'rgba(255, 206, 86, 0.8)',\n"
                + "                stack: 'Stack 1',\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor()\n"
                + "                ]\n"
                + "            }]\n"
                + "\n"
                + "        };\n"
                + "        window.onload = function() {\n"
                + "            var ctx = document.getElementById(\"stacked-group\").getContext(\"2d\");\n"
                + "            window.myBar = new Chart(ctx, {\n"
                + "                type: 'bar',\n"
                + "                data: barChartData,\n"
                + "                options: {\n"
                + "                    title:{\n"
                + "                        display:true,\n"
                + "                        text:\"Chart.js Bar Chart - Stacked\"\n"
                + "                    },\n"
                + "                    tooltips: {\n"
                + "                        mode: 'index',\n"
                + "                        intersect: false\n"
                + "                    },\n"
                + "                    responsive: true,\n"
                + "                    scales: {\n"
                + "                        xAxes: [{\n"
                + "                            stacked: true,\n"
                + "                        }],\n"
                + "                        yAxes: [{\n"
                + "                            stacked: true\n"
                + "                        }]\n"
                + "                    }\n"
                + "                }\n"
                + "            });\n"
                + "        };\n");

        col.addElement(canvas);
        col.addElement(script);
        row.addElement(col);

        col = new Div("col-sm-4");

        canvas = new Canvas();
        canvas.setId("line-styles");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        script = new Script();
        script.setType("application/javascript");
        script.setScript("var configlinestyles = {\n"
                + "            type: 'line',\n"
                + "            data: {\n"
                + "                labels: [\"January\", \"February\", \"March\", \"April\", \"May\", \"June\", \"July\"],\n"
                + "                datasets: [{\n"
                + "                    label: \"Unfilled\",\n"
                + "                    fill: false,\n"
                + "                    backgroundColor: 'rgba(54, 162, 235, 1)',\n"
                + "                    borderColor: 'rgba(54, 162, 235, 1)',\n"
                + "                    data: [\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor()\n"
                + "                    ],\n"
                + "                }, {\n"
                + "                    label: \"Dashed\",\n"
                + "                    fill: false,\n"
                + "                    backgroundColor: 'rgba(255, 206, 86, 0.2)',\n"
                + "                    borderColor: 'rgba(255, 206, 86, 1)',\n"
                + "                    borderDash: [5, 5],\n"
                + "                    data: [\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor()\n"
                + "                    ],\n"
                + "                }, {\n"
                + "                    label: \"Filled\",\n"
                + "                    backgroundColor: 'rgba(255, 99, 132, 0.2)',\n"
                + "                    borderColor: 'rgba(255, 99, 132, 1)',\n"
                + "                    data: [\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor(),\n"
                + "                        randomScalingFactor()\n"
                + "                    ],\n"
                + "                    fill: true,\n"
                + "                }]\n"
                + "            },\n"
                + "            options: {\n"
                + "                responsive: true,\n"
                + "                title:{\n"
                + "                    display:true,\n"
                + "                    text:'Chart.js Line Chart'\n"
                + "                },\n"
                + "                tooltips: {\n"
                + "                    mode: 'index',\n"
                + "                    intersect: false,\n"
                + "                },\n"
                + "                hover: {\n"
                + "                    mode: 'nearest',\n"
                + "                    intersect: true\n"
                + "                },\n"
                + "                scales: {\n"
                + "                    xAxes: [{\n"
                + "                        display: true,\n"
                + "                        scaleLabel: {\n"
                + "                            display: true,\n"
                + "                            labelString: 'Month'\n"
                + "                        }\n"
                + "                    }],\n"
                + "                    yAxes: [{\n"
                + "                        display: true,\n"
                + "                        scaleLabel: {\n"
                + "                            display: true,\n"
                + "                            labelString: 'Value'\n"
                + "                        }\n"
                + "                    }]\n"
                + "                }\n"
                + "            }\n"
                + "        };\n"
                + "\n"
                + "        window.onload = function() {\n"
                + "            var ctx = document.getElementById(\"line-styles\").getContext(\"2d\");\n"
                + "            window.myLine = new Chart(ctx, configlinestyles);\n"
                + "        };");

        col.addElement(canvas);
        col.addElement(script);

        canvas = new Canvas();
        canvas.setId("radar2");
        canvas.addAttibute("width", "100");
        canvas.addAttibute("height", "100");
        script = new Script();
        script.setType("application/javascript");
        script.setScript("var randomScalingFactor = function() {\n"
                + "        return Math.round(Math.random() * 100);\n"
                + "    };\n"
                + "\n"
                + "    var color = Chart.helpers.color;\n"
                + "    var configRadar = {\n"
                + "        type: 'radar',\n"
                + "        data: {\n"
                + "            labels: [[\"Eating\", \"Dinner\"], [\"Drinking\", \"Water\"], \"Sleeping\", [\"Designing\", \"Graphics\"], \"Coding\", \"Cycling\", \"Running\"],\n"
                + "            datasets: [{\n"
                + "                label: \"My First dataset\",\n"
                + "                backgroundColor: 'rgba(255, 99, 132, 0.2)',\n"
                + "                borderColor: 'rgba(255, 99, 132, 1)',\n"
                + "                pointBackgroundColor: 'rgba(255, 99, 132, 1)',\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor()\n"
                + "                ]\n"
                + "            }, {\n"
                + "                label: \"My Second dataset\",\n"
                + "                backgroundColor: 'rgba(54, 162, 235, 0.2)',\n"
                + "                borderColor: 'rgba(54, 162, 235, 1)',\n"
                + "                pointBackgroundColor: 'rgba(54, 162, 235, 1)',\n"
                + "                data: [\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor(),\n"
                + "                    randomScalingFactor()\n"
                + "                ]\n"
                + "            },]\n"
                + "        },\n"
                + "        options: {\n"
                + "            legend: {\n"
                + "                position: 'top',\n"
                + "            },\n"
                + "            title: {\n"
                + "                display: true,\n"
                + "                text: 'Chart.js Radar Chart'\n"
                + "            },\n"
                + "            scale: {\n"
                + "              ticks: {\n"
                + "                beginAtZero: true\n"
                + "              }\n"
                + "            }\n"
                + "        }\n"
                + "    };\n"
                + "\n"
                + "    window.onload = function() {\n"
                + "        window.myRadar = new Chart(document.getElementById(\"radar2\"), configRadar);\n"
                + "    };");

//        col.addElement(canvas);
//        col.addElement(script);

        row.addElement(col);
        content.addElement(row);

        return content.getHtml();
    }

}
