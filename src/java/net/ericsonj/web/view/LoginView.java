package net.ericsonj.web.view;

import net.ericsonj.framework.bootstrap.Container;
import net.ericsonj.html.Br;
import net.ericsonj.html.Button;
import net.ericsonj.html.Div;
import net.ericsonj.html.Form;
import net.ericsonj.html.H1;
import net.ericsonj.html.I;
import net.ericsonj.html.Img;
import net.ericsonj.html.Input;
import net.ericsonj.html.Label;
import net.ericsonj.html.Span;
import net.ericsonj.html.Strong;
import net.ericsonj.html.Text;
import net.ericsonj.framework.web.mvc.AbstractView;

/**
 *
 * @author ejoseph
 */
public class LoginView extends AbstractView {

    @Override
    public String getHtml() {

        Div container = new Div("cont_1", "container");

        Div row = new Div("row main");
        Div col = new Div("main-login main-center");

//        row.addElement(col);
//        col = new Div("col-md-4 col-sm-4");
        Form form = new Form("form-horizontal", "index.jsp", "POST");
        form.setId("loginForm");

        Div userFormGroup = new Div("form-group");
        Div userInputDiv = new Div("input-group");
        Label userLabel = new Label("Username").setFor("inputUsarname");
        userLabel.setClass("cols-sm-2 control-label");
//        userFormGroup.addElement(userLabel);

        Input userInput = new Input("inputUsarname", "form-control");
        userInput.setRequired();
        userInput.setPlaceholder("Username");
        userInput.setType("text");
        userInput.setName("username");
        userInput.addAttibute("autofocus", "autofocus");

//        userGroup.addElement(userLabel);
        Span span = new Span("input-group-addon");
        span.addElement(new I("fa fa-user fa-lg"));

        userInputDiv.addElement(span);
        userInputDiv.addElement(userInput);

        userFormGroup.addElement(userInputDiv);

        form.addElement(userFormGroup);

        Div pwsFormGroup = new Div("form-group");
        Div passwordInputDiv = new Div("input-group");
        Label passLabel = new Label("Password").setFor("inputPassword");
        passLabel.setClass("cols-sm-2 control-label");
//        pwsFormGroup.addElement(passLabel);
        Input passInput = new Input("inputPassword", "form-control");
        passInput.setRequired();
        passInput.setPlaceholder("Password");
        passInput.setType("password");
        passInput.setName("password");

//        passwordGroup.addElement(passLabel);
        span = new Span("input-group-addon");
        span.addElement(new I("fa fa-key fa-lg"));

        passwordInputDiv.addElement(span);
        passwordInputDiv.addElement(passInput);

        pwsFormGroup.addElement(passwordInputDiv);
        form.addElement(pwsFormGroup);

        if (attMap.containsKey("error")) {
            String error = (String) attMap.get("error");
            Div alert = new Div("alert alert-danger");
            alert.addElement(new Strong("Incorrect!"));
            alert.addElement(new Text(" " + error));

            form.addElement(alert);

        }

        Button button = new Button("Log in", "submit");
        button.setClass("btn btn-lg btn-primary btn-block");

        Div btFormGroup = new Div("form-group");
        btFormGroup.addElement(button);

        form.addElement(btFormGroup);

        form.setEnctype("application/x-www-form-urlencoded");

        Div rowImg = new Div("row");
        rowImg.addAttibute("align", "center");
        rowImg.addElement(new Img("ffs/img/logo.jpg").addAttibute("height", "100").addAttibute("width", "120"));
        
        col.addElement(rowImg);
        col.addElement(new Br());
        col.addElement(new Br());
        col.addElement(form);

        row.addElement(col);

        container.addElement(row);

        return container.getHtml();

    }

}
