package net.ericsonj.web.view;

import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.CRUDView;
import java.util.LinkedList;
import net.ericsonj.html.HtmlElement;
import net.ericsonj.html.I;
import net.ericsonj.html.Text;

/**
 *
 * @author ejoseph
 */
public class LocalizationCRUDView extends CRUDView {

    public LocalizationCRUDView(String opt) {
        super(opt);
    }

    @Override
    public LinkedList<ColumnDescAbstract> getHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("town_code", "DANE"));
        headers.add(new ColumnDesc("department_code", "Codigo Departamento"));
        headers.add(new ColumnDesc("department_name", "Departamento"));
        headers.add(new ColumnDesc("town_name", "Municipio"));
        return headers;
    }

    @Override
    public String getTitle() {
        return "Localizations";
    }

    @Override
    public ColumnDescAbstract getToolBar() {
        ColumnDescAbstract toolbar = new ColumnDescAbstract("", "", null, false, false, null) {
            @Override
            public String renderColumn(Class type, String jsData, String jsType, String jsRow) {
                return "'<a href=\"?opt=" + opt + "&edit&id=' + " + jsData + " + '\" class=\"btn btn-default btn-xs\">"
                        + "<span class=\"glyphicon glyphicon-pencil\"></span></a> "
                        + "<a href=\"?opt=" + opt + "&delete&id=' + " + jsData + " + '\" class=\"btn btn-danger btn-xs\">"
                        + "<span class=\"glyphicon glyphicon-trash\"></span></a>';";
            }
        };
        return toolbar;
    }

    @Override
    public I getIconForm() {
        return new I("fa fa-map-marker fa-lg");
    }

    @Override
    public HtmlElement getHeaderButtons() {
        return new Text("");
    }

}
