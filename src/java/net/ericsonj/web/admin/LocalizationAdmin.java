package net.ericsonj.web.admin;

import net.ericsonj.framework.web.mvc.CRUDController;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.bootstrap.Container;
import net.ericsonj.framework.bootstrap.TextFormField;
import net.ericsonj.html.I;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.framework.web.mvc.ViewSuccess;
import net.ericsonj.save.elements.Localization;
import net.ericsonj.save.facade.LocalizationDaoFacade;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.ModelAndView;
import net.ericsonj.framework.web.mvc.ToolItem;
import net.ericsonj.html.A;
import net.ericsonj.html.H2;
import net.ericsonj.save.elements.Privilege;
import net.ericsonj.save.elements.Profile;
import net.ericsonj.save.facade.ProfileDaoFacade;

/**
 *
 * @author ejoseph
 */
public class LocalizationAdmin extends CRUDController<Localization, Long> {

    @Override
    public LocalizationDaoFacade getAdminFacade() {
        return new LocalizationDaoFacade();
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("town_code", "DANE"));
        headers.add(new ColumnDesc("department_code", "Codigo Departamento"));
        headers.add(new ColumnDesc("department_name", "Departamento"));
        headers.add(new ColumnDesc("town_name", "Municipio"));
        return headers;
    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(Localization obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("town_code", obj.getTown_code());
        row.put("department_code", obj.getDepartment_code());
        row.put("department_name", obj.getDepartment_name());
        row.put("town_name", obj.getTown_name());
        return row;
    }

    @Override
    public void initAdminForm(AdministrableForm form, Localization loc) {

        TextFormField field1 = new TextFormField("town_code", "Codigo de municipio:");
        field1.setHelpMessage("Ingrese codigo dane de minicipio");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-2 col-md-2 col-sm-3");
        if (loc != null) {
            field1.setValue("" + loc.getTown_code());
            field1.getInput().setReadonly();
        }
        field1.getInput().addAttibute("maxlength", "5");

        form.addFormField("town_code", field1);

        field1 = new TextFormField("department_code", "Codigo Departamento:");
        field1.setHelpMessage("Ingrese codigo del departamento");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-2 col-md-2 col-sm-3");
        field1.getInput().addAttibute("maxlength", "5");
        if (loc != null) {
            field1.setValue("" + loc.getDepartment_code());
        }

        form.addFormField("department_code", field1);

        field1 = new TextFormField("department_name", "Departamento:");
        field1.setHelpMessage("Ingrese el nombre del departamento");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        field1.getInput().addAttibute("maxlength", "30");
        if (loc != null) {
            field1.setValue("" + loc.getDepartment_name());
        }

        form.addFormField("department_name", field1);

        field1 = new TextFormField("town_name", "Municipio:");
        field1.setHelpMessage("Ingrese el nombre del municipio.");
        field1.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        field1.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        field1.getInput().addAttibute("maxlength", "30");
        if (loc != null) {
            field1.setValue("" + loc.getTown_name());
        }

        form.addFormField("town_name", field1);
    }

    @Override
    public LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {
        LinkedList<ValidateException> exceptions = new LinkedList<>();
        String townCode = request.getParameter("town_code");
        System.out.println("townCode : " + townCode);
        if (!fieldValidByRegex("^[0-9]+$", townCode)) {
            exceptions.add(new ValidateException("town_code", "Ingrese solo numeros"));
        }

        String depCode = request.getParameter("department_code");
        System.out.println("department_code : " + depCode);
        if (!fieldValidByRegex("^[0-9]+$", depCode)) {
            exceptions.add(new ValidateException("department_code", "Ingrese solo numeros"));
        }

        return exceptions;
    }

    @Override
    public AbstractView ProcessEditForm(HttpServletRequest request) {

        String town_code = request.getParameter("town_code");
        String department_code = request.getParameter("department_code");
        String department_name = request.getParameter("department_name");
        String town_name = request.getParameter("town_name");

        Localization loc = new Localization();
        loc.setTown_code(Long.parseLong(town_code));
        loc.setDepartment_code(Long.parseLong(department_code));
        loc.setDepartment_name(department_name);
        loc.setTown_name(town_name);

        LocalizationDaoFacade facade = new LocalizationDaoFacade();
        facade.edit(loc);

        ViewSuccess success = new ViewSuccess("Localizations", "La localizacion fue editada exitosamente.", "index.jsp?opt=" + opt);
        success.setIconForm(getAdminIconForm());
        success.setMessageTitle(town_code);
        return success;
    }

    @Override
    public AbstractView ProcessAddForm(HttpServletRequest request) {
        String town_code = request.getParameter("town_code");
        String department_code = request.getParameter("department_code");
        String department_name = request.getParameter("department_name");
        String town_name = request.getParameter("town_name");

        Localization loc = new Localization();
        loc.setTown_code(Long.parseLong(town_code));
        loc.setDepartment_code(Long.parseLong(department_code));
        loc.setDepartment_name(department_name);
        loc.setTown_name(town_name);

        LocalizationDaoFacade facade = new LocalizationDaoFacade();
        facade.create(loc);

        ViewSuccess success = new ViewSuccess("Localizations", "La localizacion fue creada exitosamente.", "index.jsp?opt=" + opt);
        success.setIconForm(getAdminIconForm());
        success.setMessageTitle(town_code);
        return success;
    }

    @Override
    public LinkedList<ToolItem> getToolBar() {
        LinkedList<ToolItem> tb = super.getToolBar();
        ToolItem view = new ToolItem(new I("fa fa-info-circle"), "btn btn-info btn-xs", "View", "view");
        tb.add(view);
        return tb;
    }

    @Override
    public String getAdminTitle() {
        return "Localizaciones";
    }

    @Override
    public String getAddButtonLabel() {
        return "Add Localization";
    }

    @Override
    public AbstractView ProcessDeleteForm(HttpServletRequest request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public I getAdminIconForm() {
        return new I("fa fa-map-marker fa-lg");
    }

    @Override
    public String getDeleteKeyPrivilege() {
        return "DELETE_LOCALIZATION";
    }

    @Override
    public String getEditKeyPrivilege() {
        return "EDIT_LOCALIZATION";
    }

    @Override
    public String getAddKeyPrivilege() {
        return "ADD_LOCALIZATION";
    }

    @Override
    public String getViewKeyPrivilege() {
        return "VIEW_LOCALIZATION";
    }

    @Override
    public boolean requireSession() {
        return true;
    }

    @Override
    public boolean isPublicAuthorized(String keyPrivilege) {
        ProfileDaoFacade facade = new ProfileDaoFacade();
        Profile publicProfile = facade.findByProperty("name", "PUBLIC");
        if (publicProfile == null) {
            return false;
        }
        Set<Privilege> privileges = publicProfile.getPrivileges();
        for (Privilege privilege : privileges) {
            System.out.println(">> " + privilege.getPrivilege_code() + " " + keyPrivilege);
            if (keyPrivilege.equals(privilege.getPrivilege_code())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public ModelAndView customAction(String action) {
        if (isAction(action, "view")) {
            ModelAndView custom = new ModelAndView(new AbstractView() {
                @Override
                public String getHtml() {

                    Container cont = new Container();
                    cont.addElement(new H2("View " + getAdminTitle()));
                    cont.addElement(new A("Return").setHref("?opt=" + opt));

                    return cont.getHtml();
                }
            });
            return custom;
        }
        return null;
    }
}
