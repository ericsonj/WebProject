package net.ericsonj.web.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import javax.servlet.http.HttpServletRequest;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.bootstrap.CheckboxFormField;
import net.ericsonj.framework.bootstrap.MultiSelectFormField;
import net.ericsonj.framework.bootstrap.Option;
import net.ericsonj.framework.bootstrap.PasswordFormField;
import net.ericsonj.framework.bootstrap.TextFormField;
import net.ericsonj.html.I;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.save.elements.Profile;
import net.ericsonj.save.elements.User;
import net.ericsonj.save.facade.ProfileDaoFacade;
import net.ericsonj.save.facade.UserDaoFacade;
import net.ericsonj.framework.web.mvc.CRUDController;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.AdminMessageHelper;

/**
 *
 * @author ejoseph
 */
public class UserAdmin extends CRUDController<User, String> {

    @Override
    public String getAdminTitle() {
        return "Users";
    }

    @Override
    public UserDaoFacade getAdminFacade() {
        return new UserDaoFacade();
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("login", "Login"));
        headers.add(new ColumnDesc("name", "Name"));
        headers.add(new ColumnDesc("email", "Email"));
        headers.add(new ColumnDesc("deleted", "Deleted"));
        return headers;
    }

    @Override
    public void initAdminForm(AdministrableForm form, User obj) {

        TextFormField login = new TextFormField("login", "Login:");
        login.setHelpMessage("Insert login");
        login.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        login.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            login.setValue(obj.getLogin());
        }
        form.addFormField("login", login);

        TextFormField name = new TextFormField("name", "Name:");
        name.setHelpMessage("Insert Name");
        name.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        name.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            name.setValue(obj.getName());
        }
        form.addFormField("name", name);

        PasswordFormField passwd = new PasswordFormField("password", "Password:");
        passwd.setHelpMessage("Insert password");
        passwd.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        passwd.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            passwd.setValue("nopassword");
        }
        form.addFormField("password", passwd);

        PasswordFormField passwdmatcher = new PasswordFormField("password_matcher", "Comfirm Password:");
        passwdmatcher.setHelpMessage("Insert password again");
        passwdmatcher.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        passwdmatcher.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            passwdmatcher.setValue("nopassword");
        }
        form.addFormField("password_matcher", passwdmatcher);

        TextFormField email = new TextFormField("email", "Email:");
        email.setHelpMessage("Insert email");
        email.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        email.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            email.setValue(obj.getEmail());
        }
        form.addFormField("email", email);

        LinkedList<Option> options = new LinkedList<>();
        options.add(new Option("true", ""));
        CheckboxFormField isDeleted = new CheckboxFormField("Is Deleted:", "deleted", options);
        isDeleted.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        isDeleted.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            isDeleted.setValue(obj.isDeleted() ? "true" : "false");
        }

        form.addFormField("deleted", isDeleted);

        LinkedList<Option> optionsSelected = new LinkedList<>();
        LinkedList<Option> optionsAvailable = new LinkedList<>();

        List<Profile> cprofiles = new ArrayList<>();
        ProfileDaoFacade pFacade = new ProfileDaoFacade();
        Set<Profile> profilesNotDu = new LinkedHashSet<>();
        List<Profile> profiles = pFacade.findAll();
        for (Profile profile : profiles) {
            if (!profilesNotDu.contains(profile)) {
                profilesNotDu.add(profile);
            }
        }

        if (obj != null) {
            cprofiles.addAll(obj.getProfiles());
        }

        for (Profile profile : profilesNotDu) {
            System.out.println(">>> " + profile.toString());
            boolean isSelected = false;
            for (Profile c : cprofiles) {
                if (profile.getProfile_id() == c.getProfile_id()) {
                    optionsSelected.add(new Option(Long.toString(profile.getProfile_id()), profile.getName()));
                    isSelected = true;
                    break;
                }
            }
            if (!isSelected) {
                optionsAvailable.add(new Option(Long.toString(profile.getProfile_id()), profile.getName()));
            }
        }

        MultiSelectFormField msff = new MultiSelectFormField("profiles", "Profiles:", optionsAvailable, optionsSelected);
        msff.setHelpMessage("Select Profiles");
        msff.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        msff.getInputDiv().setClass("col-lg-5 col-md-5 col-sm-5");

        form.addFormField("profiles", msff);

    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(User obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("login", obj.getLogin());
        row.put("name", obj.getName());
        row.put("email", obj.getEmail());
        row.put("deleted", obj.isDeleted() ? "YES" : "NO");
        return row;
    }

    /**
     *
     * @param request
     * @return
     */
    @Override
    public LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {
        LinkedList<ValidateException> exceptions = new LinkedList<>();

        String login = request.getParameter("login");
        if (login == null || login.isEmpty()) {
            exceptions.add(new ValidateException("login", "Insert Login"));
        }

        String name = request.getParameter("name");
        if (name == null || login.isEmpty()) {
            exceptions.add(new ValidateException("name", "Insert Name"));
        }

        String email = request.getParameter("email");
        if (email == null || login.isEmpty()) {
            exceptions.add(new ValidateException("email", "Insert Email "));
        }

        String password = request.getParameter("password");
        String password_matcher = request.getParameter("password_matcher");
        if (!password.equals(password_matcher)) {
            exceptions.add(new ValidateException("password_matcher", "Password not matcher"));
        }

        return exceptions;
    }

    @Override
    public AbstractView ProcessEditForm(HttpServletRequest request) {

        String isDeleted = request.getParameter("deleted");
        String login = request.getParameter("login");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = new User();
        user.setDeleted(isDeleted != null);
        user.setLogin(login);
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);

        Set<Profile> profiles = getProfilesFromRequest(request);
        user.setProfiles(profiles);

        System.out.println(user);

        getAdminFacade().edit(user);

        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                login,
                "User edited with login: " + login,
                "?opt=" + opt);
    }

    @Override
    public AbstractView ProcessAddForm(HttpServletRequest request) {

        String isDeleted = request.getParameter("deleted");
        String login = request.getParameter("login");
        String name = request.getParameter("name");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        User user = new User();
        user.setDeleted(isDeleted != null);
        user.setLogin(login);
        user.setName(name);
        user.setEmail(email);
        user.setPassword(password);

        getAdminFacade().create(user);
        user.setPassword("nopassword");

        Set<Profile> profiles = getProfilesFromRequest(request);
        user.setProfiles(profiles);

        System.out.println(user);
        getAdminFacade().edit(user);

        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                login,
                "User add with login: " + login,
                "?opt=" + opt);
    }

    private Set<Profile> getProfilesFromRequest(HttpServletRequest request) {
        HashSet<Profile> profiles = new HashSet<>();
        String sprofiles = request.getParameter("profiles");
        if (sprofiles == null || sprofiles.isEmpty()) {
            return profiles;
        }
        StringTokenizer st = new StringTokenizer(sprofiles, ";");
        ProfileDaoFacade facade = new ProfileDaoFacade();
        while (st.hasMoreElements()) {
            Profile pAux = facade.find(Long.parseLong(st.nextToken()));
            profiles.add(pAux);
        }
        return profiles;
    }

    @Override
    public AbstractView ProcessDeleteForm(HttpServletRequest request) {
        String login = request.getParameter("id");
        User user = getAdminFacade().find(login);
        getAdminFacade().remove(user);
        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                login,
                "User delete with login: " + login,
                "?opt=" + opt);
    }

    @Override
    public I getAdminIconForm() {
        return new I("fa fa-users fa-lg");
    }

    @Override
    public String getDeleteKeyPrivilege() {
        return "DELETE_USER";
    }

    @Override
    public String getEditKeyPrivilege() {
        return "EDIT_USER";
    }

    @Override
    public String getAddKeyPrivilege() {
        return "ADD_USER";
    }

    @Override
    public String getViewKeyPrivilege() {
        return "VIEW_USER";
    }

}
