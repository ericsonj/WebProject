package net.ericsonj.web.admin;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.FinderController;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Localization;
import net.ericsonj.save.facade.LocalizationDaoFacade;

/**
 *
 * @author ejoseph
 */
public class LocalizationFinder extends FinderController<Localization> {

    @Override
    public ElementLogicFacade getAdminFacade() {
        return new LocalizationDaoFacade();
    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(Localization obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("town_code", obj.getTown_code());
        row.put("department_name", obj.getDepartment_name());
        row.put("town_name", obj.getTown_name());
        return row;
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("town_code", "DANE"));
        headers.add(new ColumnDesc("department_name", "Departamento"));
        headers.add(new ColumnDesc("town_name", "Municipio"));
        return headers;
    }

    @Override
    public String getKeyshow() {
        return "town_name";
    }

    @Override
    public String getKeyId() {
        return "town_code";
    }

    @Override
    public String getAdminTitle() {
        return "Localizations";
    }

}
