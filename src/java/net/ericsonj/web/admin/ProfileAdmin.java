package net.ericsonj.web.admin;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.bootstrap.CheckboxFormField;
import net.ericsonj.framework.bootstrap.Option;
import net.ericsonj.framework.bootstrap.TextFormField;
import net.ericsonj.html.I;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.AdminMessageHelper;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.framework.web.mvc.ViewSuccess;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Privilege;
import net.ericsonj.save.elements.Profile;
import net.ericsonj.save.facade.PrivilegeDaoFacade;
import net.ericsonj.save.facade.ProfileDaoFacade;
import net.ericsonj.framework.web.mvc.CRUDController;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import org.hibernate.HibernateException;

/**
 *
 * @author ejoseph
 */
public class ProfileAdmin extends CRUDController<Profile, Long> {

    @Override
    public String getAdminTitle() {
        return "Profiles";
    }

    @Override
    public ElementLogicFacade getAdminFacade() {
        return new ProfileDaoFacade();
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("profile_id", "Id"));
        headers.add(new ColumnDesc("name", "Name"));
        headers.add(new ColumnDesc("editable", "Editable"));
        return headers;
    }

    @Override
    public void initAdminForm(AdministrableForm form, Profile obj) {

        TextFormField name = new TextFormField("name", "Name:");
        name.setHelpMessage("Insert Name");
        name.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        name.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            name.setValue(obj.getName());
        }
        form.addFormField("name", name);

        LinkedList<Option> options = new LinkedList<>();
        options.add(new Option("true", ""));
        CheckboxFormField isEditable = new CheckboxFormField("Is Editable:", "editable", options);
        isEditable.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        isEditable.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            isEditable.setValue(obj.isEditable() ? "true" : "false");
        }

        form.addFormField("deleted", isEditable);

        String privilegesValues = "";
        if (obj != null) {
            Set<Privilege> cprPrivileges = obj.getPrivileges();
            for (Privilege cprPrivilege : cprPrivileges) {
                privilegesValues += cprPrivilege.getPrivilege_code();
            }
        }

        PrivilegeDaoFacade facade = new PrivilegeDaoFacade();
        HashSet<String> categories = new HashSet<>();
        List<Privilege> privileges = facade.findAll();
        for (Privilege privilege : privileges) {
            if (!categories.contains(privilege.getCategory())) {
                categories.add(privilege.getCategory());
            }
        }

        for (String categorie : categories) {

            LinkedList<Option> optionsCategory = new LinkedList<>();
            for (Privilege privilege : privileges) {
                if (privilege.getCategory().equals(categorie)) {
                    optionsCategory.add(new Option(privilege.getPrivilege_code(), privilege.getDescription()));
                }
            }
            CheckboxFormField cheackCategory = new CheckboxFormField(categorie, "privileges", optionsCategory);
            cheackCategory.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
            cheackCategory.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
            form.addFormField("privileges_" + categorie, cheackCategory);
            cheackCategory.setValue(privilegesValues);
        }

    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(Profile obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("profile_id", obj.getProfile_id());
        row.put("name", obj.getName());
        row.put("editable", obj.isEditable() ? "YES" : "NO");
        return row;
    }

    @Override
    public LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {
        LinkedList<ValidateException> exceptions = new LinkedList<>();
        String name = request.getParameter("name");
        if (name.isEmpty()) {
            exceptions.add(new ValidateException("name", "Insert name"));
        }
        return exceptions;
    }

    @Override
    public AbstractView ProcessEditForm(HttpServletRequest request) {

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String editable = request.getParameter("editable");
        String[] arrayPriv = request.getParameterValues("privileges");

        System.out.println(id);
        System.out.println(name);
        System.out.println(editable);
        System.out.println(Arrays.toString(arrayPriv));

        ProfileDaoFacade facade = new ProfileDaoFacade();
        Profile profile = facade.find(Long.parseLong(id));
        profile.setName(name);
        profile.setEditable(editable != null);

        Set<Privilege> privileges = new HashSet<>();

        if (arrayPriv != null) {

            PrivilegeDaoFacade privilegeFacade = new PrivilegeDaoFacade();
            for (String code : arrayPriv) {
                Privilege p = privilegeFacade.find(code);
                if (p != null) {
                    privileges.add(p);
                }
            }
        }

        profile.setPrivileges(privileges);
        facade.edit(profile);

//        ViewSuccess success = new ViewSuccess("Profiles", "Profile Edited", "?opt=" + opt);
//        success.setIconForm(getAdminIconForm());
//        success.setMessageTitle(name);
//        return success;
        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                name,
                "Profile edited",
                "?opt=" + opt);
    }

    @Override
    public AbstractView ProcessAddForm(HttpServletRequest request) {

        String name = request.getParameter("name");
        String editable = request.getParameter("editable");
        String[] arrayPriv = request.getParameterValues("privileges");

        System.out.println(name);
        System.out.println(editable);

        Profile profile = new Profile(name, editable != null);
        ProfileDaoFacade facade = new ProfileDaoFacade();

        if (arrayPriv != null) {
            PrivilegeDaoFacade privilegeFacade = new PrivilegeDaoFacade();
            Set<Privilege> privileges = new HashSet<>();
            for (String code : arrayPriv) {
                Privilege p = privilegeFacade.find(code);
                if (p != null) {
                    privileges.add(p);
                }
            }
            System.out.println(privileges.toString());
            profile.setPrivileges(privileges);
        }

        facade.create(profile);

        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                name,
                "Profile added",
                "?opt=" + opt);

    }

    @Override
    public AbstractView ProcessDeleteForm(HttpServletRequest request) throws Exception {
        String profile_id = request.getParameter("id");
        Profile profile = (Profile) getAdminFacade().find(Long.parseLong(profile_id));
        try {
            getAdminFacade().remove(profile);
        } catch (HibernateException ex) {
            throw new HibernateException("This profile is use. You can't delete.");
        }
        return AdminMessageHelper.getOKMessage(
                getAdminIconForm(),
                getAdminTitle(),
                profile.getName(),
                "Profile deleted with " + profile_id,
                "?opt=" + opt);
    }

    @Override
    public I getAdminIconForm() {
        return new I("fa fa-id-card-o fa-lg");
    }

    @Override
    public String getDeleteKeyPrivilege() {
        return "DELETE_PROFILE";
    }

    @Override
    public String getEditKeyPrivilege() {
        return "EDIT_PROFILE";
    }

    @Override
    public String getAddKeyPrivilege() {
        return "ADD_PROFILE";
    }

    @Override
    public String getViewKeyPrivilege() {
        return "VIEW_PROFILE";
    }

}
