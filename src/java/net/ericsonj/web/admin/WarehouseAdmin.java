package net.ericsonj.web.admin;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.bootstrap.FinderFormField;
import net.ericsonj.framework.bootstrap.TextFormField;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.AdminMessageHelper;
import net.ericsonj.framework.web.mvc.CRUDController;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.ColumnForeignDesc;
import net.ericsonj.framework.web.mvc.ColumnSearchText;
import net.ericsonj.framework.web.mvc.ForeignHelp;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.html.HtmlElement;
import net.ericsonj.html.I;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Localization;
import net.ericsonj.save.elements.Warehouse;
import net.ericsonj.save.facade.LocalizationDaoFacade;
import net.ericsonj.save.facade.WarehouseDaoFacade;

/**
 *
 * @author ericson
 */
public class WarehouseAdmin extends CRUDController<Warehouse, Long> {

    @Override
    public String getAdminTitle() {
        return "Warehouse";
    }

    @Override
    public ElementLogicFacade getAdminFacade() {
        return new WarehouseDaoFacade();
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("id", "Id"));
        headers.add(new ColumnDesc("name", "Name"));
        headers.add(new ColumnForeignDesc("l", "department_name", "Department"));
        headers.add(new ColumnForeignDesc("l", "town_name", "Town", String.class, new ColumnSearchText()));
        return headers;
    }

    @Override
    public void initAdminForm(AdministrableForm form, Warehouse obj) {

        TextFormField name = new TextFormField("name", "Name:");
        name.setHelpMessage("Ingrese el nombre de warehouse");
        name.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        name.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        name.getInput().addAttibute("maxlength", "30");
        if (obj != null) {
            name.setValue("" + obj.getName());
        }
        form.addFormField("name", name);

        FinderFormField finder = new FinderFormField("localization", "Localization:", new LocalizationFinder());
        finder.setHelpMessage("Seleccione un municipio.");
        finder.getLabel().setClass("col-lg-5 col-md-5 col-sm-5");
        finder.getInputDiv().setClass("col-lg-4 col-md-4 col-sm-4");
        if (obj != null) {
            finder.setValue(obj.getLocalization().getTown_name());
            finder.setHiddenValue(String.valueOf(obj.getLocalization().getTown_code()));
        }

        form.addFormField("localization", finder);

    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(Warehouse obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("id", obj.getId());
        row.put("name", obj.getName());
        row.put(ForeignHelp.getKey("l", "department_name"), obj.getLocalization().getDepartment_name());
        row.put(ForeignHelp.getKey("l", "town_name"), obj.getLocalization().getTown_name());
        return row;
    }

    @Override
    public LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {
        LinkedList<ValidateException> exceptions = new LinkedList<>();

        String name = request.getParameter("name");
        if (name == null || name.isEmpty()) {
            exceptions.add(new ValidateException("name", "Debe ingresar un nombre."));
        }
        String localization = request.getParameter("localization");
        if (localization == null || localization.isEmpty()) {
            exceptions.add(new ValidateException("localization", "Debe ingresar una lozalizacion."));
        }

        return exceptions;
    }

    @Override
    public AbstractView ProcessEditForm(HttpServletRequest request) throws Exception {

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String localization = request.getParameter("localization");
        LocalizationDaoFacade lFacade = new LocalizationDaoFacade();
        Localization loc = lFacade.find(Long.parseLong(localization));

        Warehouse warehouse = ((WarehouseDaoFacade) getAdminFacade()).find(Long.parseLong(id));
        warehouse.setName(name);
        warehouse.setLocalization(loc);
        getAdminFacade().edit(warehouse);
        return AdminMessageHelper.getOKMessage(getAdminIconForm(), getAdminTitle(), name, "Warehouse with " + name + " was edited.", "?opt=" + opt);
    }

    @Override
    public AbstractView ProcessAddForm(HttpServletRequest request) throws Exception {
        String name = request.getParameter("name");
        String localization = request.getParameter("localization");
        LocalizationDaoFacade lFacade = new LocalizationDaoFacade();
        Localization loc = lFacade.find(Long.parseLong(localization));
        Warehouse w = new Warehouse(name, loc);
        getAdminFacade().create(w);
        return AdminMessageHelper.getOKMessage(getAdminIconForm(), getAdminTitle(), name, "Warehouse with " + name + " is added.", "?opt=" + opt);
    }

    @Override
    public AbstractView ProcessDeleteForm(HttpServletRequest request) throws Exception {
        String id = request.getParameter("id");
        Warehouse warehouse = ((WarehouseDaoFacade) getAdminFacade()).find(Long.parseLong(id));
        getAdminFacade().remove(warehouse);
        return AdminMessageHelper.getOKMessage(getAdminIconForm(), getAdminTitle(), warehouse.getName(), "Warehouse with " + warehouse.getName() + " was remobed.", "?opt=" + opt);
    }

    @Override
    public HtmlElement getAdminIconForm() {
        return new I("fa fa-home fa-lg");
    }

    @Override
    public String getDeleteKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getEditKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getAddKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getViewKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getAdminSelectQuery(String selectQuery, String where) {
        String query = selectQuery + " as w left join localizations as l on (w.town_code = l.town_code ) " + where;
        System.out.println("::: " + query);
        return query;
    }

}
