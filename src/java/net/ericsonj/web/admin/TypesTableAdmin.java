package net.ericsonj.web.admin;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import javax.servlet.http.HttpServletRequest;
import net.ericsonj.framework.bootstrap.AdministrableForm;
import net.ericsonj.framework.web.mvc.AbstractView;
import net.ericsonj.framework.web.mvc.CRUDController;
import net.ericsonj.framework.web.mvc.ColumnDesc;
import net.ericsonj.framework.web.mvc.ColumnDescAbstract;
import net.ericsonj.framework.web.mvc.ColumnSearchDate;
import net.ericsonj.framework.web.mvc.ColumnSearchSelect;
import net.ericsonj.framework.web.mvc.ColumnSearchText;
import net.ericsonj.framework.web.mvc.ValidateException;
import net.ericsonj.html.HtmlOption;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.TypesTable;
import net.ericsonj.save.facade.TypesTableDaoFacade;

/**
 *
 * @author ejoseph
 */
public class TypesTableAdmin extends CRUDController<TypesTable, Long> {

    @Override
    public String getAdminTitle() {
        return "TypesTable";
    }

    @Override
    public ElementLogicFacade getAdminFacade() {
        return new TypesTableDaoFacade();
    }

    @Override
    public LinkedList<ColumnDescAbstract> getAdminHeaders() {
        LinkedList<ColumnDescAbstract> headers = new LinkedList<>();
        headers.add(new ColumnDesc("id", "ID"));
        headers.add(new ColumnDesc("type_decimal", "Decimal", String.class, new ColumnSearchText()));
        headers.add(new ColumnDesc("type_timestamp", "Timestamp", Timestamp.class));
        headers.add(new ColumnDesc("type_date", "Date", Date.class, new ColumnSearchDate()));

        LinkedList<HtmlOption> options = new LinkedList<>();
        options.add(new HtmlOption("", ""));
        options.add(new HtmlOption("0", "false"));
        options.add(new HtmlOption("1", "true"));
        ColumnSearchSelect cs = new ColumnSearchSelect(options);

        headers.add(new ColumnDesc("type_boolean", "Boolean", Boolean.class, cs));

        return headers;
    }

    @Override
    public void initAdminForm(AdministrableForm form, TypesTable obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LinkedHashMap<String, Object> getAdminRow(TypesTable obj) {
        LinkedHashMap<String, Object> row = new LinkedHashMap<>();
        row.put("id", obj.getId());
        row.put("type_decimal", obj.getType_decimal());
        row.put("type_timestamp", obj.getType_timestamp());
        row.put("type_date", obj.getType_date());
        row.put("type_boolean", obj.isType_boolean());
        return row;
    }

    @Override
    public LinkedList<ValidateException> validRequestForm(HttpServletRequest request) {
        LinkedList<ValidateException> exceptions = new LinkedList<>();
        return exceptions;
    }

    @Override
    public AbstractView ProcessEditForm(HttpServletRequest request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractView ProcessAddForm(HttpServletRequest request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public AbstractView ProcessDeleteForm(HttpServletRequest request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getDeleteKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getEditKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getAddKeyPrivilege() {
        return "ALL";
    }

    @Override
    public String getViewKeyPrivilege() {
        return "ALL";
    }

}
