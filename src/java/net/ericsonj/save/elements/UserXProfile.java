package net.ericsonj.save.elements;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ejoseph
 */
@Entity
@Table(name = "user_x_profile")
public class UserXProfile implements Serializable {

    @Id
    private String login;
    private long profile_id;

    public UserXProfile() {
    }

    public UserXProfile(String login, long profile) {
        this.login = login;
        this.profile_id = profile;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public long getProfile_id() {
        return profile_id;
    }

    public void setProfile_id(long profile_id) {
        this.profile_id = profile_id;
    }

    @Override
    public String toString() {
        return "UserXProfile{" + "login=" + login + ", profile=" + profile_id + '}';
    }

}
