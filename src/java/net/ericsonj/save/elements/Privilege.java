package net.ericsonj.save.elements;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ejoseph
 */
@Entity
@Table(name = "privilege")
public class Privilege implements Serializable {
    
    @Id
    private String privilege_code;
    private String description;
    private String category;

    public Privilege() {
    }

    public Privilege(String privilege_code, String description, String category) {
        this.privilege_code = privilege_code;
        this.description = description;
        this.category = category;
    }
    
    public String getPrivilege_code() {
        return privilege_code;
    }

    public void setPrivilege_code(String privilege_code) {
        this.privilege_code = privilege_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Provilege{" + "privilege_code=" + privilege_code + ", description=" + description + ", category=" + category + '}';
    }
    
}
