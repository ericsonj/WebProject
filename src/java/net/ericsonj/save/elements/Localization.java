package net.ericsonj.save.elements;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ejoseph
 */
@Entity
@Table(name = "localizations")
public class Localization implements Serializable {
    
    @Id
    private long town_code;
    private long department_code;
    private String department_name;
    private String town_name;

    public Localization() {
    }

    public Localization(long town_code, long department_code, String department_name, String town_name) {
        this.town_code = town_code;
        this.department_code = department_code;
        this.department_name = department_name;
        this.town_name = town_name;
    }

    public long getTown_code() {
        return town_code;
    }

    public void setTown_code(long town_code) {
        this.town_code = town_code;
    }

    public long getDepartment_code() {
        return department_code;
    }

    public void setDepartment_code(long department_code) {
        this.department_code = department_code;
    }

    public String getDepartment_name() {
        return department_name;
    }

    public void setDepartment_name(String department_name) {
        this.department_name = department_name;
    }

    public String getTown_name() {
        return town_name;
    }

    public void setTown_name(String town_name) {
        this.town_name = town_name;
    }

    @Override
    public String toString() {
        return "Localization{" + "town_code=" + town_code + ", department_code=" + department_code + ", department_name=" + department_name + ", town_name=" + town_name + '}';
    }
    
    
}
