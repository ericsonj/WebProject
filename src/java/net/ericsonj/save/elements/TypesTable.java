package net.ericsonj.save.elements;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author ejoseph
 */
@Entity
@Table(name = "types_table")
public class TypesTable implements Serializable {

    @Id
    private long id;
    private float type_decimal;
    private Timestamp type_timestamp;
    private Date type_date;
    private boolean type_boolean;

    public TypesTable() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public float getType_decimal() {
        return type_decimal;
    }

    public void setType_decimal(float type_decimal) {
        this.type_decimal = type_decimal;
    }

    public Timestamp getType_timestamp() {
        return type_timestamp;
    }

    public void setType_timestamp(Timestamp type_timestamp) {
        this.type_timestamp = type_timestamp;
    }

    public Date getType_date() {
        return type_date;
    }

    public void setType_date(Date type_date) {
        this.type_date = type_date;
    }

    public boolean isType_boolean() {
        return type_boolean;
    }

    public void setType_boolean(boolean type_boolean) {
        this.type_boolean = type_boolean;
    }
    
}
