package net.ericsonj.save.elements;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author ericson
 */
@Entity
@Table(name = "warehouse")
public class Warehouse implements Serializable {

    @Id
    private long id;
    private String name;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "town_code")
    private Localization localization;

    public Warehouse() {
    }

    public Warehouse(String name, Localization localization) {
        this.name = name;
        this.localization = localization;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Localization getLocalization() {
        return localization;
    }

    public void setLocalization(Localization localization) {
        this.localization = localization;
    }

    @Override
    public String toString() {
        return "Warehouse{" + "id=" + id + ", name=" + name + ", localization=" + localization + '}';
    }

}
