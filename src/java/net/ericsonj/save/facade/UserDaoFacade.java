package net.ericsonj.save.facade;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.User;
import org.apache.commons.codec.digest.Crypt;
import org.hibernate.HibernateException;

/**
 *
 * @author ejoseph
 */
public class UserDaoFacade extends ElementLogicFacade<User, String> {

    public boolean validateUser(String username, String password) throws HibernateException {
        User user = find(username);
        if (user == null) {
            return false;
        }

        String storagePws = user.getPassword();
        Pattern p = Pattern.compile("^[$][0-9]+[$][a-zA-Z0-9./]+[$]");
        Matcher m = p.matcher(storagePws);
        String salt = "";
        if (m.find()) {
            salt = m.group();
        }
        String cryp = Crypt.crypt(password, salt);
        return cryp.equals(storagePws);
    }

    @Override
    public String create(User Entity) throws HibernateException {
        String pws = Crypt.crypt(Entity.getPassword());
        Entity.setPassword(pws);
        return super.create(Entity); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void edit(User Entity) throws HibernateException {
        if (Entity.getPassword().equals("nopassword")) {
            User old = find(Entity.getLogin());
            Entity.setPassword(old.getPassword());
        } else {
            String pws = Crypt.crypt(Entity.getPassword());
            Entity.setPassword(pws);
        }
        super.edit(Entity); //To change body of generated methods, choose Tools | Templates.
    }

}
