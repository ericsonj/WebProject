package net.ericsonj.save.facade;

import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Warehouse;

/**
 *
 * @author ericson
 */
public class WarehouseDaoFacade extends ElementLogicFacade<Warehouse, Long> {

}
