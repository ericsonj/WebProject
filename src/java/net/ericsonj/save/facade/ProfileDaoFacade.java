package net.ericsonj.save.facade;


import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Profile;

/**
 *
 * @author ejoseph
 */
public class ProfileDaoFacade extends ElementLogicFacade<Profile, Long> {

}
