package net.ericsonj.save.facade;

import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Localization;

/**
 *
 * @author ejoseph
 */
public class LocalizationDaoFacade extends ElementLogicFacade<Localization, Long> {

    public void truncateTable() {
        super.executeQuery("TRUNCATE TABLE " + getTableName());
    }

}
