package net.ericsonj.save.facade;

import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.TypesTable;

/**
 *
 * @author ejoseph
 */
public class TypesTableDaoFacade extends ElementLogicFacade<TypesTable, Long> {

}
