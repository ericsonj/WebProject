package net.ericsonj.save.facade;

import net.ericsonj.save.ElementLogicFacade;
import net.ericsonj.save.elements.Privilege;

/**
 *
 * @author ejoseph
 */
public class PrivilegeDaoFacade extends ElementLogicFacade<Privilege, String> {

}
