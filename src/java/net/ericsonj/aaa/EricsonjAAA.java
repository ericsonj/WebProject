package net.ericsonj.aaa;

import net.ericsonj.save.elements.User;
import net.ericsonj.save.facade.UserDaoFacade;

/**
 *
 * @author ejoseph
 */
public class EricsonjAAA implements Authorizable {

    private final String username;
    private final String password;
    private AAAUser aaaUser;

    public EricsonjAAA(String username, String password) {
        this.username = username;
        this.password = password;
        this.aaaUser = null;
    }

    @Override
    public AUser getUserObject() {
        return this.aaaUser;
    }

    @Override
    public boolean isAuthorised() {
        UserDaoFacade userFacade = new UserDaoFacade();
        boolean authorized = userFacade.validateUser(username, password);
        if(authorized){
            User user = userFacade.find(username);
            this.aaaUser = new AAAUser(user);
        }
        return authorized;
    }

}
