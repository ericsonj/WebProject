package net.ericsonj.aaa;

import java.util.Set;
import net.ericsonj.save.elements.Privilege;
import net.ericsonj.save.elements.Profile;
import net.ericsonj.save.elements.User;

/**
 *
 * @author ejoseph
 */
public class AAAUser extends AUser {

    public AAAUser(User user) {
        super(user);
    }

    @Override
    public boolean userCan(String key) {
        User user = (User) getUserObject();
        Set<Profile> profiles = user.getProfiles();
        for (Profile profile : profiles) {
            Set<Privilege> privileges = profile.getPrivileges();
            for (Privilege privilege : privileges) {
                if(privilege.getPrivilege_code().equals("ALL")){
                    return true;
                }
                if (privilege.getPrivilege_code().equals(key)) {
                    return true;
                }
            }
        }
        return false;
    }

}
